# 11ty SSG CMS

A template site using Eleventy Static Site Generator with Netlify CMS

# How to Use

Copy or fork this repository into a new repo.

Deploy to netlify.

Add a domain name.

Add identity.
- Set registration to invite only
- Turn on Git Gateway
- Invite users

Add redirects.
