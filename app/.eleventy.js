module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy('www/assets');
  eleventyConfig.addPassthroughCopy('www/css');
  eleventyConfig.addPassthroughCopy('www/admin');

  return {
    passthroughFileCopy: true,
    dir: {
      input: "www",
      layouts: "/_layouts",
    }
  }
}
