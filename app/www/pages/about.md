---
layout: page
title: About
permalink: /about/
---
# About

![Picture of Zac Littleberry. Headshot standing on a bridge, the sun on their face. No eye contact.](/assets/uploads/zaclittleberry_profile-photo.jpg)

Gaze.Dev is a web development and technical consulting company incorporated by Zac Littleberry (as Gaze LLC) in 2018. Zac has been doing web administration, web development, software development, marketing, advising, and teaching for eight years.

Zac started his career at Pittsburgh Filmmakers/Pittsburgh Center for the Arts (PF/PCA) first as a Web Developer in 2012, then becoming Web Administrator in 2014. At PF/PCA, Zac rebuilt the web infrastructure of PF/PCA to make the websites more reliable, easier to maintain updates, and allowed departments to manage their own content. This new infrastructure powered Pittsburgh Filmmakers, Pittsburgh Center for the Arts, Pittsburgh Arts, Filmmakers Theaters, the Three Rivers Film Festival, the Pittsburgh Biennial, and an internal staff website. Zac also helped revamp the marketing email infrastructure to be a better experience for both marketing and users.

After PF/PCA, Zac joined Arsenal Studios as a web developer, working with a variety of clients to understand their problems and come up with solutions. From 2016 to 2018, Zac worked at the Linux Foundation where he started working on the Collaborative Projects team, maintaining and redeveloping websites for The Linux Foundation's many projects. He then moved to the internal platforms team where he worked on The Linux Foundation’s new company wide internal platform, as well as a contributor legal compliance system that was later released as EasyCLA.

After leaving the Linux Foundation, Zac founded and focused on Gaze.dev, helping clients such as 412 Food Rescue, XQ Institute, and Carnegie Mellon University’s CREATE Lab.

In addition to web development work, Zac has a passion for teaching, advising, and mentorship. At PF/PCA, Zac taught Web & Web Apps, a program to teach foundational web development to highschool students, and Android App Fundamentals, a highschool level program focused on the basics of programming Android Apps with MIT App Inventor 2. Zac also taught Minecraft Club, a program for children ages 11-13 to learn the basics of programming through the game Minecraft. Zac also did substitute teaching for Intro to Web Design. Zac served as technical advisor to a local startup, involveMINT, between 2018 and 2019. Since starting Gaze.Dev, Zac has also facilitated five educational internships and five Carnegie Mellon University students through their Capstone project.

Zac has a passion for using technology to advance the social good. Some of his personal projects include HousingDB, a modular, open-source platform for understanding and improving rental housing. He also created PittsburghHousing.org, an instance of HousingDB, which allows renters to review their rental properties and management companies and also serves as a repository for housing-related resources.