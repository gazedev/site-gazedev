---
layout: page
title: Home
permalink: /
---
<section id="our-services" class="main">
<h2>Our Services</h2>

<ol>
<li>
<img src="/assets/uploads/services_product-development.png" alt="idea to concept">

<h3>Product Development</h3>

<p class="subheading">What do you want to build?</p>

<p>You have a problem that needs a solution. We can turn that idea into a product concept, with feature descriptions and technical capabilities.</p>

</li>

<li>

<img src="/assets/uploads/services_project-management.png" alt="items in a list">

<h3>Project Management</h3>

<p class="subheading">What are the steps to build it?</p>

<p>We’ll help you figure out the steps to make your concept happen, creating a road map to get from point A to point B in a way that is fast and high quality, with an eye for efficiency. </p>

</li>
<li>

<img src="/assets/uploads/services_software-development.png" alt="markup on a screen">

<h3>Software Development</h3>

<p class="subheading">Let’s build it.</p>

<p>We can take your plan and put into action with scalable, high quality software solutions, done right.</p>

<!--p>We’ll help you figure out what technology is best for your needs, but here are some technologies that we skilled in:</p>
Add logos with wordmarks from skills section of resume
If large section, have it live somewhere else and link to it.-->
</li>
<li>
<img src="/assets/uploads/services_technical-consulting.png" alt="questionmark to exclamation point">

<h3>Technical Consulting and Capacity Building </h3>

<p class="subheading">"I don’t know where to start."</p>

<p>Not sure what you need? We can work with you to figure out what your needs are and how to best fill in those gaps.</p>

<p><strong>We're offering 1 hour of consultation for free. <a href="#schedule-a-meeting">Schedule a meeting</a>.</strong></p>
</li>
</ol>
</section>

<section id="about">
<h2>About</h2>

<img src="/assets/uploads/zaclittleberry_profile-photo.jpg">

<p>
Gaze.Dev is a web development and technical consulting company incorporated by Zac Littleberry (as Gaze LLC) in 2018. Zac has been doing web administration, web development, software development, marketing, advising, and teaching for eight years. <a href="/about" class="read-more-link">Read More</a>
</p>

</section>

<section id="clients">
<h2>Clients</h2>
<ul>
<li>
<a target="_blank" rel="noopener noreferrer" href="https://cmucreatelab.org/">
<img id="logo_create-lab" src="/assets/uploads/cmu-create-lab.svg" alt=" logo">
<p>CMU CREATE Lab</p>
</a>
</li>
<li>
<a target="_blank" rel="noopener noreferrer" href="https:/412FoodRescue.org/">
<img id="logo_412foodrescue" src="/assets/uploads/412FoodRescue.png" alt=" logo">
<p>412 Food Rescue</p>
</a>
</li>
<li>
<a target="_blank" rel="noopener noreferrer" href="https://xqsuperschool.org/">
<img id="logo_xq-institute" src="/assets/uploads/XQ.png" alt=" logo">
<p>XQ Institute</p>
</a>
</li>
<li>
<a target="_blank" rel="noopener noreferrer" href="https://basezero.com/">
<img id="logo_basezero" src="/assets/uploads/basezero.svg" alt=" logo">
<p>basezero</p>
</a>
</li>
<li>
<a target="_blank" rel="noopener noreferrer" href="https://latentai.com/">
<img id="logo_latentai" src="/assets/uploads/LatentAI.jpeg" alt=" logo">
<p>LatentAI</p>
</a>
</li>
<li>
<a target="_blank" rel="noopener noreferrer" href="https://www.involvemint.io/">
<img id="logo_involvemint" src="/assets/uploads/involvemint.svg" alt=" logo">
<p>involveMINT</p>
</a>
</li>
</ul>
</section>

<section id="selected-projects">
<h2>Selected Projects</h2>

<div class="projects">

<div class="project">
<div class="frame browser">
<img src="/assets/uploads/screenshot-pittsburgh-housing-desktop.png" alt="screenshot of Pittsburgh Housing App" />
</div>
<h3>Pittsburgh Housing App</h3>

<p>
Pittsburgh Housing is a web application that provides listings, ratings, and reviews of landlords and properties. The web app acts as a rental registry of properties in Allegheny County, and repository of rental housing resources. In the app, users can fill out forms to submit properties and landlords, search through data in the system, see properties on maps, and login to leave reviews.
</p>

<p>
<strong>Features:</strong> Forms, Maps, User Logins, APIs
</p>
</div>

<div class="project">
<div class="frame browser">
<img src="/assets/uploads/screenshot-owner-search-desktop.png" alt="Screenshot of Owner Search" />
</div>
<h3>Owner Search</h3>

<p>
In the Owner Search tool, users can search through multiple data sets to see a variety of information including property ownership, assessment data, building code violations, and corporation information. Data is pre-processed to support discovery of property and corporation ownership networks. Due to the sensitive nature of information in the tool, access is limited to select nonprofits, organizations, and government entities.
</p>

<p>
<strong>Features:</strong> Web Scraping, Data Processing, User Logins, Restricted Content, APIs
</p>
</div>

<div class="project">
<div class="frame browser">
<img src="/assets/uploads/screenshot-quick-website-starter-kit-desktop.png" alt="Screenshot of Quick Website Starter Kit" />
</div>
<h3>Quick Website Starter Kit</h3>

<p>
With the Quick Website Starter Kit, users can customize website content through an easy-to-use content management system. The Quick Website Starter Kit compiles changes to static pages and deploys them to a content delivery network. This means that compared to other content management systems such as WordPress, these sites load fast, have low ongoing maintenance costs, and lower server costs as site traffic increases. Best of all, because of the skillful caching of the network, there’s no delay in changes showing up once they have been deployed.
</p>

<p>
<strong>Features:</strong> Website Builder, Analytics, Netlify Content Management System, 11ty.dev
</p>
</div>

</div>

</section>

<section id="schedule-a-meeting">
<h2>Schedule a Meeting</h2>

<p>We do free 1-hour consultations. Schedule a time for us to do a video call and talk about what you want.</p>

<p><strong><a href="https://calendly.com/gazedev" target="_blank">Schedule a time to meet via Calendly</a>.</strong></p>

</section>

<section>
<h2>Contact Us</h2>

<p>Please <a href="mailto:gazepgh+webcontact@gmail.com" target="_blank">contact us via email</a>.

</section>
